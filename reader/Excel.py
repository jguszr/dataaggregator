from openpyxl import load_workbook
from openpyxl.utils import column_index_from_string
from openpyxl.utils import coordinate_from_string
from setup import Config

dataRead = {}
masterGambi = False

def openWorkBook(fileRef):
    return load_workbook(fileRef, data_only=True)



def setDataSizeBasedOnRange(cfg,ws):

    start = coordinate_from_string(cfg.split(":")[0])
    finish = coordinate_from_string(cfg.split(":")[1])
    colStart = column_index_from_string(start[0])
    colEnd = column_index_from_string(finish[0])
    rowStart = ws[cfg.split(":")[0]].row
    rowEnd = ws[cfg.split(":")[1]].row
    resp = (abs(1+ colEnd - colStart), abs(1+ rowEnd - rowStart))

    return resp


def assertKeyForrange(k, f):

    if k["use_filename_as_id"] == "True":
        return f + "_" + k["id"]
    else:
        return k["id"]

def readRangesOfData(wb,f):

    ranges = Config.read_strategy["ranges"]

    for k in ranges:
        print("processing sheet : " + k["from_sheet"], " from file :", f, " with range", k["simple_range"])
        try:
            ws = wb.get_sheet_by_name(k["from_sheet"])
            i = 0
            resp = setDataSizeBasedOnRange(k["simple_range"], ws)
            data = [[0 for y in range(resp[0])] for x in range(resp[1])]
            for row in ws.iter_rows(k["simple_range"]):
                content = []
                col = 0
                for cel in row:
                    content.append(cel.value)
                    data[i][col] = cel.value
                    col +=1
                i += 1

            dataRead[assertKeyForrange(k, f)] = data

        except KeyError:
            print("Sheet not found")


def getNumOfRows(dataRead):

    ret = 0
    global masterGambi
    totalRows = 0
    if masterGambi:
        for k in dataRead:
            print("tamanho de dataRead[k]", len(dataRead[k]))
            ret += len(dataRead[k])
        masterGambi = True
        totalRows +=ret
        return totalRows

    masterGambi = True
    return ret

def writeData():
    wb = openWorkBook(Config.outbound_file)
    print("cell reference ",Config.write_strategy["cell_reference"])
    print("writing to file :", Config.outbound_file  )
    print("writing to Sheet", Config.outbound_sheet)

    try:
        ws = wb.get_sheet_by_name(Config.outbound_sheet)
        colIndex = 0
        rowIndex = ws[Config.write_strategy["cell_reference"]].row + getNumOfRows(dataRead)
        print("row index: ",rowIndex)
        for k in dataRead.keys():
            colIndex = column_index_from_string(ws[Config.write_strategy["cell_reference"]].column)
            content = dataRead[k]
            ws.cell(row=rowIndex, column=colIndex).value = k
            for c in content:
                colIndex = column_index_from_string(ws[Config.write_strategy["cell_reference"]].column)
                for r in c:
                    colIndex +=1
                    ws.cell(row=rowIndex, column=colIndex).value = r

                rowIndex +=1
    except KeyError:
        print("Sheet not found")

    return wb


def processAll():
    global dataRead
    for f in Config.to_be_read:
        print("Reading file :" + f)
        readRangesOfData(openWorkBook(f), f)
        print("Writing as fast as RR Martin...")
        wb = writeData()
        dataRead = {}
        wb.save(Config.outbound_file)

