from openpyxl import load_workbook

from reader import Excel
from setup import Config




if __name__ == '__main__':
    Config.loadConfigFile()
    Config.listExcelFiles()
    Excel.processAll()