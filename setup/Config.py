import json
import logging
import os
import pathlib

cfg_file = str(pathlib.Path.cwd().parent) + os.sep + os.sep + "cfg" + os.sep + "config.json"


db_path = ""
inbound_sheet = ""
outbound_file = ""
outbound_sheet = ""
read_strategy = {}
to_be_read = []
write_strategy = {}

def loadJsonConfig():

    global db_path, inbound_sheet
    global outbound_file
    global outbound_sheet
    global read_strategy
    global write_strategy

    print("loadJsonConfig()")
    with open(cfg_file) as data_file:
        data = json.load(data_file)
        db_path = data["dbpath"]
        outbound_file = data["output_file"]
        outbound_sheet = data["output_sheet"]
        read_strategy = data["read_strategy"]
        write_strategy = data["write_strategy"]
        showParams()


def loadConfigFile():

    if(os.path.exists(cfg_file)):
        loadJsonConfig()
    else:
        print("pau")

def listExcelFiles():

    global to_be_read
    for f in os.listdir(db_path):
        if f.endswith("xlsx"):
            to_be_read.append(db_path + f)

def validateDbPath():
    return os.path.exists(db_path)

def validateOutboundFile():
    return os.path.isfile(outbound_file)

def showParams():

    print("*" * 30)
    print("Configured params")
    print("*" * 30)
    print("db path : " + db_path)
    print("inbound sheet : " + inbound_sheet)
    print("outbound file : " + outbound_file)
    print("outbound sheet : " + outbound_sheet)
    print("*" * 30)

if __name__ == '__main__':
    loadConfigFile()
    print("validate DB : " + str(validateDbPath()))
    print("validate outbound file : " + str(validateOutboundFile()))
    print(os.listdir(db_path))